#!/usr/bin/env bash
set -e

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

touch docker/www/version.json
echo "{ \"version\": ${TARGET_VERSION:0:7} }" > docker/www/version.json

docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/xivoxc_nginx:$TARGET_VERSION docker/

rm docker/www/version.json