#
# Nginx proxy for xivoxc
#
# Required aliases:
# * kibana
# * xuc
# * xucmgt
# * spagobi
# * recording
# * elasticsearch (needed in Deneb for compat with Kibana 3)
#
# Required environment variables:
# !!! List of environment variables must correspond with "envsubst" in Dockerfile !!!
# * NGINX_XIVO_HOST
# * NGINX_XUC_HOST
# * NGINX_XUC_PORT
# * NGINX_XUCMGT_HOST
# * NGINX_XUCMGT_PORT
# * NGINX_ELASTICSEARCH_HOST (needed in Deneb for compat with Kibana 3)
# * NGINX_KIBANA_HOST
# * NGINX_SPAGOBI_HOST
# * NGINX_SPAGOBI_PORT
# * NGINX_RECORDING_SERVER_HOST
# * NGINX_RECORDING_SERVER_PORT
#
# Required mounts:
# * /usr/share/kibana
# * /etc/nginx/ssl
#

server {
  listen                80 default_server;

  access_log            /var/log/nginx/xivocc.org.access.log;

  return 301 https://$host$request_uri;
}

# ----------------------------
# XUC SSL + webRTC wss:
# * overview page, sample etc.
# ----------------------------
server {
    listen 8443 ssl;
    gzip off;
    ssl_certificate /etc/nginx/ssl/xivoxc.crt;
    ssl_certificate_key /etc/nginx/ssl/xivoxc.key;
    ssl_ciphers DEFAULT;

    location /ws {
        proxy_pass http://${NGINX_XIVO_HOST}:5039;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_buffering off;
        proxy_connect_timeout 60m;
        proxy_read_timeout 60m;
        proxy_send_timeout 60m;
        keepalive_timeout 180s;
    }

    location / {
        proxy_pass  http://${NGINX_XUC_HOST}:${NGINX_XUC_PORT}/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /xuc/ {
        proxy_pass http://${NGINX_XUC_HOST}:${NGINX_XUC_PORT}/xuc/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_buffering off;
        proxy_connect_timeout 60m;
        proxy_read_timeout 60m;
        proxy_send_timeout 60m;
    }
}

#-----------------
# SSL + wss
# ----------------
server {
    listen 443 ssl;
    gzip off;
    ssl_certificate /etc/nginx/ssl/xivoxc.crt;
    ssl_certificate_key /etc/nginx/ssl/xivoxc.key;
    ssl_ciphers DEFAULT;

    #----------------------------------
    # XUCMGT SSL + XUC wss + webRTC wss
    # ---------------------------------
    location /ws {
        proxy_pass http://${NGINX_XIVO_HOST}:5039;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_buffering off;
        proxy_connect_timeout 60m;
        proxy_read_timeout 60m;
        proxy_send_timeout 60m;
        keepalive_timeout 180s;
    }

    include /etc/nginx/sip_proxy/sip_proxy[.]conf;

    location / {
        proxy_pass  http://${NGINX_XUCMGT_HOST}:${NGINX_XUCMGT_PORT}/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /xuc/ {
        proxy_pass http://${NGINX_XUC_HOST}:${NGINX_XUC_PORT}/xuc/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_buffering off;
        proxy_connect_timeout 60m;
        proxy_read_timeout 60m;
        proxy_send_timeout 60m;
    }

    location /popc {
        rewrite ^ /switchboard permanent;
    }

    #----------------------------------
    # CONFIGMGT SSL
    # ---------------------------------
    location ~ ^/configmgt.*$ {
        proxy_pass https://${NGINX_XIVO_HOST};
        proxy_connect_timeout 180s;
        proxy_read_timeout 180s;
        proxy_send_timeout 180s;
    }

    #----------------------------------
    # RECORDING SSL
    # ---------------------------------
    location ~ ^/recording.*$ {
        proxy_pass http://${NGINX_RECORDING_SERVER_HOST}:${NGINX_RECORDING_SERVER_PORT};
        proxy_connect_timeout 180s;
        proxy_read_timeout 180s;
        proxy_send_timeout 180s;
    }

    # ------------------------
    # * Version --------------
    # ------------------------
    location /version {
      alias /usr/share/nginx/html/version.json;
      add_header Content-Type application/json;
      expires -1;
    }

      # ------------------------
  # * Fingerboard ----------
  # ------------------------
  location /fingerboard {
	alias /usr/share/nginx/html;
	index index.html index.htm;
  }

  # ------------------------
  # * Kibana 3 -------------
  # ------------------------
  location /preKibana {
    alias /usr/share/kibana/;
  }

  location ~ ^/_aliases$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
  }
  location ~ ^/.*/_aliases$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
  }
  location ~ ^/_nodes$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
  }
  location ~ ^/.*/_search$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
  }
  location ~ ^/.*/_mapping {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
  }

  # Password protected end points
  location ~ ^/kibana-int/dashboard/.*$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
    limit_except GET {
      proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
      auth_basic "Restricted";
      auth_basic_user_file /etc/nginx/conf.d/kibana.xivoxc.htpasswd;
    }
  }
  location ~ ^/kibana-int/temp.*$ {
    proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
    proxy_read_timeout 90;
    limit_except GET {
      proxy_pass http://${NGINX_ELASTICSEARCH_HOST}:9200;
      auth_basic "Restricted";
      auth_basic_user_file /etc/nginx/conf.d/kibana.xivoxc.htpasswd;
    }
  }

  # ------------------------
  # * Kibana ---------------
  # ------------------------
  location ~ ^/kibana(.*)$ {
    rewrite /kibana(/)?(.*) /$2 break;                                
    proxy_pass http://${NGINX_KIBANA_HOST}:5601;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }

  # ------------------------
  # * SpagoBI --------------
  # ------------------------
  location ~ ^/SpagoBI.*$ {
    proxy_pass http://${NGINX_SPAGOBI_HOST}:${NGINX_SPAGOBI_PORT};
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }

  # ------------------------
  # * Webpanel -------------
  # ------------------------
  location /webpanels {
    alias /usr/share/webpanels/;
  }

}
